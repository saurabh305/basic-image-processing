#include"stdafx.h"
#include <iostream>
#include <opencv2/opencv.hpp>

#include "AnnotateImages.h"
//
//int main(){
//  cv::Mat im = cv::imread("path/to/an/image");
//  
//  
//  // --- Points ---
//  // left-click: add a point
//  // right-click: delete the last added point
//  // middle-click: delete all points
//  // space: close window and return vector of points
//  // esc: close window and return empty vector
//  Annotate<cv::Point2f> ui4Points(im);
//  // get max of 4 points from user - the string is appended to the window name
//  std::vector<cv::Point2f> pts = ui4Points.getItems(4, "Anti-clockwise from bottom-left");
//  
//  // get unlimited number of points from user - the string is appended to the window name
//  std::vector<cv::Point2f> pts = ui4Points.getItems(0, "Anti-clockwise from bottom-left");
//  
//  
//  
//  // --- Rectangles ---
//  // left-click drag: draw rectangle
//  // right-click: delete the last added rectangle
//  // middle-click: delete all rectangles
//  // space: close window and return vector of rectangles
//  // esc: close window and return empty vector
//  Annotate<cv::Rect> ui4Rects(im);
//  // get max of 6 rectangles drawn by user - the string is appended to the window name
//  std::vector<cv::Rect> boxes = ui4Rects.getItems(6, "Draw ROIs");
//  
//  
//  
//  // --- Straight Lines ---
//  // left-click drag: draw line
//  // right-click: delete the last added line
//  // middle-click: delete all lines
//  // space: close window and return vector of lines
//  // esc: close window and return empty vector
//  Annotate<line> ui4Lines(im);
//  // get max of 3 lines drawn by user - the string is appended to the window name
//  std::vector<line> lines = ui4Lines.getItems(3, "Draw 3 Lines");
//  
//  
//  
//  // --- Polygons ---
//  // left-click: add new vertex
//  // right-click: close polygon (if drawing) / delete the last added vertex
//  // middle-click: delete all polygons
//  // space: close window and return vector of polygons
//  // esc: close window and return empty vector
//  Annotate<Polygon> ui4Polygon(im);
//  // get max of 2 polygons drawn by user - the string is appended to the window name
//  std::vector<Polygon> polygons = ui4Polygon.getItems(2, "Draw 2 Polygons");
//  
//  
//  return 0;
//}