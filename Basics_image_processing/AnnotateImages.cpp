//
// Created by sumit on 20/9/17.
//
#include"stdafx.h"
#include "AnnotateImages.h"


// ----------- draw --------------
template <> void Annotate<cv::Point_<int>>::draw(cv::Mat &showImage) {
    for(int i=0; i<mArray.size(); i++)
        cv::circle(showImage, mArray[i], 3, cv::Scalar(0, 250, 0), -1);
}

template <> void Annotate<cv::Point_<float>>::draw(cv::Mat &showImage) {
    for(int i=0; i<mArray.size(); i++)
        cv::circle(showImage, mArray[i], 3, cv::Scalar(0, 250, 0), -1);
}

template <> void Annotate<line>::draw(cv::Mat &showImage) {
    for(int i=0; i<mArray.size(); i++)
        cv::line(showImage, mArray[i].p1, mArray[i].p2, cv::Scalar(0, 250, 0), 2);
}

template <> void Annotate<cv::Rect>::draw(cv::Mat &showImage) {
    for(int i=0; i<mArray.size(); i++)
        cv::rectangle(showImage, mArray[i], cv::Scalar(0, 250, 0), 2);
}

template <> void Annotate<Polygon>::draw(cv::Mat &showImage) {
    if(mArray.empty()) return;
    for (int i=0; i<mArray.size(); i++) {
        Polygon p = mArray[i];
        p.draw(showImage);
    }
}


// ---------------- window name ------------------
template <> std::string Annotate<cv::Point_<int>>::windowName(int numberOfItems, std::string remark) {
    std::ostringstream ss;
    if(numberOfItems > 0) ss << "UI | Click " << numberOfItems << " Points | " + remark;
    else ss << "UI | Draw Points | " + remark;

    cv::namedWindow(ss.str());
    cv::setMouseCallback(ss.str(), callbacks::points, this);
    return ss.str();
}

template <> std::string Annotate<cv::Point_<float>>::windowName(int numberOfItems, std::string remark) {
    std::ostringstream ss;
    if(numberOfItems > 0) ss << "UI | Click " << numberOfItems << " Points | " + remark;
    else ss << "UI | Draw Points | " + remark;

    cv::namedWindow(ss.str());
    cv::setMouseCallback(ss.str(), callbacks::points2f, this);
    return ss.str();
}

template <> std::string Annotate<line>::windowName(int numberOfItems, std::string remark) {
    std::ostringstream ss;
    if(numberOfItems > 0) ss << "UI | Draw " << numberOfItems << " Lines | " + remark;
    else ss << "UI | Draw Lines | " + remark;
    cv::namedWindow(ss.str());
    cv::setMouseCallback(ss.str(), callbacks::lines, this);
    return ss.str();
}

template <> std::string Annotate<cv::Rect>::windowName(int numberOfItems, std::string remark) {
    std::ostringstream ss;
    if(numberOfItems > 0) ss << "UI | Draw " << numberOfItems << " Boxes | " + remark;
    else ss << "UI | Draw Boxes | " + remark;
    cv::namedWindow(ss.str());
    cv::setMouseCallback(ss.str(), callbacks::boxes, this);
    return ss.str();
}

template <> std::string Annotate<Polygon>::windowName(int numberOfItems, std::string remark) {
    std::ostringstream ss;
    if(numberOfItems > 0) ss << "UI | Draw " << numberOfItems << " Polygons | " + remark;
    else ss << "UI | Draw Polygons | " + remark;
    cv::namedWindow(ss.str());
    cv::setMouseCallback(ss.str(), callbacks::Polygons, this);
    return ss.str();
}

//------------------ CallBacks -------------------

void callbacks::points(int event, int x, int y, int flags, void *param) {
    Annotate<cv::Point_<int>>* ui = (Annotate<cv::Point_<int>>*)param;
    switch(event){
        case cv::EVENT_LBUTTONUP: ui->addItem(cv::Point(x,y)); break;
        case cv::EVENT_RBUTTONUP: ui->removeLastItem(); break;
        case cv::EVENT_MBUTTONUP: ui->getArray()->clear(); break;
        default:;
    }
}

void callbacks::points2f(int event, int x, int y, int flags, void *param) {
    Annotate<cv::Point_<float>>* ui = (Annotate<cv::Point_<float>>*)param;
    switch(event){
        case cv::EVENT_LBUTTONUP: ui->addItem(cv::Point(x,y)); break;
        case cv::EVENT_RBUTTONUP: ui->removeLastItem(); break;
        case cv::EVENT_MBUTTONUP: ui->getArray()->clear(); break;
        default:;
    }
}

void callbacks::lines(int event, int x, int y, int flags, void *param) {
    Annotate<line>* ui = (Annotate<line>*)param;
    static bool drag = false;
    switch(event){
        case cv::EVENT_LBUTTONDOWN: if(!drag) { line l; l.p1 = cv::Point(x, y); l.p2 = cv::Point(x,y); ui->addItem(l); drag = true; } break;
        case cv::EVENT_LBUTTONUP: if(drag) { drag = false; } break;
        case cv::EVENT_MOUSEMOVE: if(drag && !ui->getArray()->empty()) { ui->getArray()->back().p2 = cv::Point(x,y); } break;
        case cv::EVENT_RBUTTONUP: { ui->removeLastItem(); drag = false;}
        case cv::EVENT_MBUTTONUP: ui->getArray()->clear(); break;
        default:;
    }
}

void callbacks::boxes(int event, int x, int y, int flags, void *param) {
    Annotate<cv::Rect>* ui = (Annotate<cv::Rect>*)param;
    static cv::Rect r;
    static bool drag = false;
    switch(event){
        case cv::EVENT_LBUTTONDOWN: if(!drag) { r = cv::Rect(cv::Point(x,y), cv::Point(x,y)); ui->addItem(r); drag = true; } break;
        case cv::EVENT_LBUTTONUP: if(drag) { drag = false; } break;
        case cv::EVENT_MOUSEMOVE: if(drag && !ui->getArray()->empty()) { ui->getArray()->back() = cv::Rect(r.tl(), cv::Point(x,y)); } break;
        case cv::EVENT_RBUTTONUP: { ui->removeLastItem(); drag = false;} break;
        case cv::EVENT_MBUTTONUP: ui->getArray()->clear(); break;
        default:;
    }
}


void callbacks::Polygons(int event, int x, int y, int flags, void *param) {
    Annotate<Polygon>* ui = (Annotate<Polygon>*)param;
    static bool drag = false;

    switch(event){
        case cv::EVENT_LBUTTONUP:
            if(!drag) {
                Polygon poly;
                poly.addVertex(cv::Point2f(x,y)); poly.addVertex(cv::Point2f(x,y));
                ui->addItem(poly);
                if((ui->getArray()->back().getMaxVertices() > 0) && (ui->getArray()->back().getMaxVertices() > ui->getArray()->back().getPolygonVertices()->size())){ // delete oldest vertex on overflow
                    ui->getArray()->back().getPolygonVertices()->erase(ui->getArray()->back().getPolygonVertices()->begin());
                }
                drag = true;
            }
            else {
                Polygon* poly = &ui->getArray()->back();
                poly->getPolygonVertices()->back() = cv::Point2f(x,y);
                poly->addVertex(cv::Point2f(x,y));

                if((ui->getArray()->back().getMaxVertices() > 0) && (ui->getArray()->back().getMaxVertices() > ui->getArray()->back().getPolygonVertices()->size())){ // delete oldest vertex on overflow
                    ui->getArray()->back().getPolygonVertices()->erase(ui->getArray()->back().getPolygonVertices()->begin());
                }
            }
            break;

        case cv::EVENT_MOUSEMOVE:
            if(drag && !ui->getArray()->empty()) {
                Polygon* poly = &ui->getArray()->back();
                poly->getPolygonVertices()->back() = cv::Point2f(x,y);
            } break;

        case cv::EVENT_RBUTTONUP:
            if(drag) { drag = false; }
            if(!ui->getArray()->empty()) {
                Polygon *poly = &ui->getArray()->back();
                poly->removeLastVertex();
                if(poly->getPolygonVertices()->size() < 2) { ui->removeLastItem(); drag = false; }
            } break;

        case cv::EVENT_MBUTTONUP: { ui->getArray()->clear(); drag=false; } break;

        default:;
    }

    if(!ui->getArray()->empty()) ui->getArray()->back().lastDrawingFinished = !drag;

}