//
// Created by sumit on 20/9/17.
//

#ifndef ANNOTATE_H
#define ANNOTATE_H

#include <iostream>
#include <opencv2/opencv.hpp>

/**
 * @details
 * left-click to place a point, right-click to remove last placed point/line, middle-click to clear everything and start fresh
 * space to return the annotated points as vector
 * esc to cancel and return empty vector
 */

namespace callbacks {
    void points(int event, int x, int y, int flags, void* param);
    void points2f(int event, int x, int y, int flags, void *param);
    void lines(int event, int x, int y, int flags, void* param);
    void boxes(int event, int x, int y, int flags, void* param);
    void Polygons(int event, int x, int y, int flags, void *param);
}

struct line {
    cv::Point2f p1, p2;
    double m, c;
};


struct Polygon {

private:
    int maxVertices = -1;
    std::vector<cv::Point2f> points;
    cv::Scalar color = cv::Scalar(0, 250, 0);
    int lineThickness = 2;

public:
    Polygon(){}
    Polygon(const int vx) : maxVertices(vx) {}

    bool lastDrawingFinished = true;

    void setMaxVertices(const int x) { maxVertices = x; }
    int getMaxVertices() const { return maxVertices; }
    void addVertex(cv::Point2f p) { points.push_back(p); }
    void removeLastVertex() { points.pop_back(); }
    std::vector<cv::Point2f>* getPolygonVertices() { return &points; }

    void draw(cv::Mat &im){
        if(lastDrawingFinished) drawClosed(im);
        else drawOpen(im);
    }

    void drawOpen(cv::Mat &im) {
        if(points.size() > 1)
            for (int i=0;i<points.size()-1;i++) {
                cv::line(im, points[i], points[i+1], color, lineThickness);
            }
    }

    void drawClosed(cv::Mat &im) {
        if(points.size()>1){
            drawOpen(im);
            cv::line(im, points[0], points.back(), color, lineThickness);
        }
    }
};

template <typename T>
class Annotate {
private:
    cv::Mat mImage;
    std::vector<T> mArray;
    unsigned int mFrameInterval=25;

public:

    Annotate() {};
    Annotate(cv::Mat im) : mImage(im) {}
    void setImage(cv::Mat im) { mImage = im; }

    void addItem(T item) { mArray.push_back(item); }
    void removeLastItem() { if(!mArray.empty()) mArray.pop_back(); }
    std::vector<T>* getArray() { return &mArray; }

    std::vector<T> getItems(int numberOfItems, std::string remark=""){
        std::string winName = windowName(numberOfItems, remark);

        mArray.clear(); // start fresh

        cv::Mat showImage;
        bool loop = true;
        while(loop){
            showImage = mImage.clone();
            if((numberOfItems > 0) && (mArray.size() > numberOfItems)) mArray.erase(mArray.begin()); // remove oldest element on overflow
            draw(showImage);
            cv::imshow(winName, showImage);
            char key = cv::waitKey(mFrameInterval);
            switch(key){
                case 27: cv::destroyWindow(winName); return std::vector<T>(); // escape key
                case 32: loop = false; break; // space key
                default: ;
            }
        }
        cv::destroyWindow(winName);
        return mArray;
    }
    void getItems(int numberOfItems, std::vector<T> &outArray, std::string remark="") { outArray = getItems(numberOfItems, remark); }

    void draw(cv::Mat&);
    std::string windowName(int numberOfItems, std::string remark);
};



#endif //ANNOTATE_H
