// Basics_image processing.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include<opencv2/opencv.hpp>
#include"AnnotateImages.h"

using namespace std;
using namespace cv;

int r1, b1, g1, rl, gl, bl;
Mat threshold(Mat img , int thres)
{
	uchar ch;
	Mat binary(img.size(), CV_8UC1, Scalar(0));
	for (int cols = 0; cols < img.cols; cols++)
	{
		for (int rows = 0; rows < img.rows; rows++)
		{
			ch = img.at<uchar>(rows, cols);
			if (ch>=thres)
			  {			
						binary.at<uchar>(rows, cols) = 255;
			     }
			else
			     {
			         	binary.at<uchar>(rows, cols) = 0;
			        }
		  }
	 }
	return binary;
}

void threshold_color(Mat img, int thres_blue,int thres_green,int thres_red)
{
	Vec3b ch;
	Mat binary(img.size(), CV_8UC1, Scalar(0));
	for (int cols = 0; cols < img.cols; cols++)
	{
		for (int rows = 0; rows < img.rows; rows++)
		{
			ch = img.at<Vec3b>(rows, cols);
			if (ch[0]>=thres_blue && ch[1]>=thres_green && ch[2]<=thres_red)
			{
				binary.at<uchar>(rows, cols) = 255;
			}
		    else
		    {
				binary.at<uchar>(rows, cols) =0;
			}
		}
	}
	cv::imshow("Original Image", img);
	cv::imshow("Binary Image", binary);
}
void threshold_trackbar(Mat img, int* thres_blue, int* thres_green, int* thres_red)
{
	Vec3b ch;
	Mat binary(img.size(), CV_8UC1, Scalar(0));
	for (int i = 0; i < img.rows; i++)
	{
		for (int j = 0; j < img.cols; j++)
		{
			ch = img.at<Vec3b>(i, j);
			if (ch[0]<*thres_blue)
			{
				if (ch[1]<*thres_green)
				{
					if (ch[2]>*thres_red)
					{
						binary.at<uchar>(i, j) = 255;

					}
				}
			}
			else
			{
				binary.at<uchar>(i, j) = 0;
			}
		}
	}
	imshow("Binary Image", binary);
}


void brightness_increase(Mat img, int bright_val)
{
	Mat img2(img.size(), CV_8UC3, Scalar(0)),img3(img.size(), CV_8UC3, Scalar(0));
	for (int cols = 0; cols < img.cols; cols++)
	{
		for (int rows = 0; rows < img.rows; rows++)
		{
		img3.at<Vec3b>(rows, cols)[0] = img.at<Vec3b>(rows, cols)[0] +bright_val;
		img3.at<Vec3b>(rows, cols)[1] = img.at<Vec3b>(rows, cols)[1] +bright_val;
		img3.at<Vec3b>(rows, cols)[2] = img.at<Vec3b>(rows, cols)[2] +bright_val;

		if (img3.at<Vec3b>(rows, cols)[0] <= 255)
		{
			img2.at<Vec3b>(rows, cols)[0] = img3.at<Vec3b>(rows, cols)[0];
		}
		else
		{
			img2.at<Vec3b>(rows, cols)[0] = 255;
		}

		if (img3.at<Vec3b>(rows, cols)[1] <= 255)
		{
			img2.at<Vec3b>(rows, cols)[1] = img3.at<Vec3b>(rows, cols)[1];
		}
		else
		{
			img2.at<Vec3b>(rows, cols)[1] = 255;
		}

		if (img3.at<Vec3b>(rows, cols)[2] <= 255)
		{
			img2.at<Vec3b>(rows, cols)[2] = img3.at<Vec3b>(rows, cols)[2];
		}
		else
		{
			img2.at<Vec3b>(rows, cols)[2] = 255;
		}

	  }
	}
	cv::imshow("Original image", img);
	cv::imshow("Brighter image",img2);
}
Mat inserted_image(Mat img1, Mat img2)
{
	int cols, rows;
	Mat img3 = img1.clone();
	int img2_cols = img2.size().width, img2_rows = img2.size().height;
	for (cols = 0; cols < img2_cols; cols++)
	     {
	        	for (rows = 0; rows < img2_rows; rows++)
	            	{
	             		img3.at<Vec3b>(rows, cols)[0] = img2.at<Vec3b>(rows, cols)[0];
	                		img3.at<Vec3b>(rows, cols)[1] = img2.at<Vec3b>(rows, cols)[1];
		                    	img3.at<Vec3b>(rows, cols)[2] = img2.at<Vec3b>(rows, cols)[2];
		                }
	         }
	return img3;
}
Mat crop_image(Mat img1)
{
	int l = 0, m = 0, cols, rows;
    Annotate<cv::Rect> ui4Rects(img1);
	vector<cv::Rect> boxes = ui4Rects.getItems(1, "Draw ROIs");
	cout << "\n x coordinate ->" << boxes[0].x << "  y coordinate ->" << boxes[0].y;
	int cols_new= boxes[0].width, rows_new = boxes[0].height;
	Mat img2(rows_new,cols_new, CV_8UC3, Scalar(0));
	for (cols = boxes[0].x;  cols< boxes[0].x + boxes[0].width; cols++, l++)
	      {
	        for (rows= boxes[0].y; rows < boxes[0].y + boxes[0].height; rows++,m++)
	          {
	                img2.at<Vec3b>(m, l)[0] = img1.at<Vec3b>(rows, cols)[0];
	                  img2.at<Vec3b>(m, l)[1] = img1.at<Vec3b>(rows, cols)[1];
                    	img2.at<Vec3b>(m, l)[2] = img1.at<Vec3b>(rows, cols)[2];
	             }
			m = 0;
	         }
	    return img2;
}

Mat blend_image(Mat img1,Mat img2)
{
	int rows, cols;
	Mat img3 = Mat(img1.size(), CV_8UC3, Scalar(0));
	    for (cols = 0; cols < img2.size().width; cols++)
	       {
			for (rows = 0; rows < img2.size().height; rows++)
		         {
			        img3.at<Vec3b>(rows, cols)[0] = img1.at<Vec3b>(rows, cols)[0] * 0.6 + img2.at<Vec3b>(rows, cols)[0]* 0.4;
		        	img3.at<Vec3b>(rows, cols)[1] = img1.at<Vec3b>(rows, cols)[1] * 0.6 + img2.at<Vec3b>(rows, cols)[1] * 0.4;
	     	        img3.at<Vec3b>(rows, cols)[2] = img1.at<Vec3b>(rows, cols)[2] * 0.6 + img2.at<Vec3b>(rows, cols)[2] * 0.4;
		         }
	       }
		return img3;
}
Mat object_recog(Mat img1, Mat img2)
{
	int cols, rows;
	Mat img3=Mat(img1.size(), CV_8UC3, Scalar(0));
	for (cols = 0; cols < img1.size().width; cols++)
	{
		for (rows = 0; rows < img1.size().height; rows++)
		{
			img3.at<Vec3b>(rows, cols)[0] = img1.at<Vec3b>(rows, cols)[0] - img2.at<Vec3b>(rows, cols)[0];
			img3.at<Vec3b>(rows, cols)[1] = img1.at<Vec3b>(rows, cols)[1] - img2.at<Vec3b>(rows, cols)[1];
			img3.at<Vec3b>(rows, cols)[2] = img1.at<Vec3b>(rows, cols)[2] - img2.at<Vec3b>(rows, cols)[2];
		}
	}
	//imshow("Processed Image", img3);
	return img3;
}
int label, parent[10];
void union_(int x, int m, int parent[])
{
	int i = x;
	int j = m;
	while (parent[i] != 0)
		i = parent[i];
	while (parent[j] != 0)
		j = parent[j];
	if (i != j)
	{
		parent[j] = i;
	}

}

void count_object(Mat img)
{
	int label = 0,m,a[2];
	Mat labeled_image(img.size(), CV_8UC1, Scalar(0));
	//uchar val;
	//int north, south, east, west;
	for (int i = 1; i < 10; i++)
	{
		parent[i] = 0;
	}
	for (int rows = 0; rows < img.rows; rows++)
	{
		label = 0;
			for (int cols = 0; cols < img.cols; cols++)
			{
				if (img.at<uchar>(rows, cols) == 1)
				{
					a[0] = img.at<uchar>(rows - 1, cols);
					a[1] = img.at<uchar>(rows, cols - 1);

					if (a[0] == 0 && a[1] == 0)
					{
						m = label;
						label = label + 1;
					}
					else
					{
						if (a[0] < a[1])
							m = a[0];
						else
							m = a[1];
					}
					labeled_image.at<uchar>(rows, cols) = m;
					union_(a[0], m, parent);
					union_(a[1], m, parent);
				}
			}
		
			/*val = img.at<uchar>(rows, cols);
			if (val == 1)
			{
				labeled_image.at<uchar>(rows, cols) = 1;
				north = img.at<uchar>(rows - 1, cols);
				south = img.at<uchar>(rows + 1, cols);
				east = img.at<uchar>(rows, cols + 1);
				west = img.at<uchar>(rows, cols - 1);
				//label = 1;
				if (north == 1)
				{
					labeled_image.at<uchar>(rows - 1, cols) = 1;
				}
				if (south == 1)
				{
					labeled_image.at<uchar>(rows + 1, cols) = 1;
				}
				if (east == 1)
				{
					labeled_image.at<uchar>(rows, cols + 1) = 1;
				}
				if (west == 1)
				{
					labeled_image.at<uchar>(rows, cols - 1) = 1;
				}
				label = 1;
			}
				else
				{
					continue;
					label = label + 1;
				}*/
			imshow("labeled Images", labeled_image);
		}
	}

	int main()
	{
		int r1, r2,cols,rows,choice;
		Mat img1_m, img2_m,img3_m,img4_m,img5_m,img6_m,img7_m,img8_m,img9_m;
		namedWindow("Changed");
		cout << "\nBasic Image Processing program ->";
		cout << "\n1.Convert Gray Scale image into Binary ";
		cout << "\n2.Convert the RBG image to Binary";
		cout << "\n3.Apply Trackbar to Image";
		cout << "\n4.Increse Brightness of Image";
		cout << "\n5.Insert the given Image onto another one";
		cout << "\n6.Crop the given Image";
		cout << "\n7.Blend two given Image";
		cout << "\n8.Object recognition in two images";
		cout << "\n9.Counting the number of objects in image"<<endl;
		cin >> choice;
		namedWindow("Changed");
		switch (choice)
		{
		case 1:img1_m = imread("C:\\Users\\saurabh chandra\\Desktop\\minion.jpg");
                        cvtColor(img1_m, img2_m, COLOR_BGR2GRAY);
						imshow("Grey Scale Image",img2_m);
						img3_m=threshold(img2_m, 150);
						imshow("Binary image",img3_m);
						break;
		case 2:img1_m= imread("C:\\Users\\saurabh chandra\\Desktop\\minion.jpg");
		            	threshold_color(img1_m, 20, 50, 150);
						break;
		case 3:img1_m= imread("C:\\Users\\saurabh chandra\\Desktop\\minion.jpg");
			           createTrackbar("Red", "Changed", &b1, 255, threshold_trackbar);
			           createTrackbar("Blue", "Changed", &g1, 255, threshold_trackbar);
		               createTrackbar("Green", "Changed", &r1, 255, threshold_trackbar);
					   threshold_trackbar(img,&b1,&g1,&r1);
					   break;
		case 4:img1_m= imread("C:\\Users\\saurabh chandra\\Desktop\\minion.jpg");
			            brightness_increase(img1_m, 50);
					    break;
		case 5:img1_m= imread("C:\\Users\\saurabh chandra\\Desktop\\Inside-Out.jpg");
			   img2_m =imread("C:\\Users\\saurabh chandra\\Desktop\\minion.jpg"); 
			            img3_m = inserted_image(img1_m, img2_m);
			            imshow("Inserted Image", img3_m);
						break;
		case 6:img1_m= imread("C:\\Users\\saurabh chandra\\Desktop\\Inside-Out.jpg");
			            img2_m=crop_image(img1_m);
						imshow("Croped image", img2_m);
						break;
		case 7:img1_m = imread("C:\\Users\\saurabh chandra\\Desktop\\minion.jpg");
			   img2_m = imread("C:\\Users\\saurabh chandra\\Desktop\\Inside-Out_1.jpg");
			            img3_m = blend_image(img1_m, img2_m);
			            imshow("Blended Image", img3_m);
						break;
		case 8:img1_m = imread("C:\\Users\\saurabh chandra\\Desktop\\12.png");
			   img2_m = imread("C:\\Users\\saurabh chandra\\Desktop\\13.png");
			            img3_m = object_recog(img1_m, img2_m);
						imshow("Processed Image", img3_m);
						break;
		case 9:img1_m = imread("C:\\Users\\saurabh chandra\\Desktop\\16.jpg");
			           //  cvtColor(img1_m, img2_m, COLOR_BGR2GRAY);
			             img3_m = threshold(img2_m, 150);
						// imshow("Binary Image", img2_m);
                         count_object(img3_m);
						imshow("original image", img1_m);
						break;
		default:cout << "You have an error";
		}
			
		waitKey(0);
		return 0;
	}
